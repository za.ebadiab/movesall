package advance.android.movesall;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.util.List;

import advance.android.movesall.MVP.Contract;
import advance.android.movesall.MVP.Presenter;
import advance.android.movesall.service.LatLngListDBService;
import advance.android.movesall.utils.BaseActivity;
import advance.android.movesall.utils.PublicMethods;

public class ActivityMain extends BaseActivity implements Contract.View {

    Presenter presenter = new Presenter(this);
    String TAG = "mtest";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        bind();
        checkPermission();

    }


    void bind() {
        findViewById(R.id.btnStartLocationListService).setOnClickListener(v -> {
            presenter.onStartLocationServiceSelected();
        });

        findViewById(R.id.btnStopLocationListService).setOnClickListener(v -> {
            presenter.onStopLocationServiceSelected();

        });

        findViewById(R.id.btnShowLocationOnMap).setOnClickListener(v -> {

            presenter.onStartMapActivitySelected();
        });

        findViewById(R.id.btnDeleteDB).setOnClickListener(v -> {
            presenter.onDeleteTableValuesSelected();
        });
    }


    void checkPermission() {
   //     if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION

                ).withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {

                PublicMethods.Companion.toast(getString(R.string.GPS_permitted));
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {


            }

        }).check();

    }


    @Override
    public void startLocationService() {
        Log.d(TAG, "startLocationService: before Intent");
        startService(new Intent(mContext, LatLngListDBService.class));

    }

    @Override
    public void stopLocationService() {
        stopService(new Intent(mContext, LatLngListDBService.class));

    }

    @Override
    public void startMapActivity() {
        startActivity(new Intent(mContext, MapsActivity.class));

    }
}
