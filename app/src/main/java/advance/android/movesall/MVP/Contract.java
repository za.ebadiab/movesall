package advance.android.movesall.MVP;

import advance.android.movesall.MVPMap.ContractMap;

public interface Contract {

    interface View {
        void startLocationService();

        void stopLocationService();

        void startMapActivity();

    }

    interface Presenter {
        void onStartLocationServiceSelected();

        void onStopLocationServiceSelected();

        void onStartMapActivitySelected();

        void onDeleteTableValuesSelected();


    }

    interface Model {

        void attachPresenter(Presenter presenter);

        void deleteTableValues();

        void attachPresenter(ContractMap.Presenter presenter);

        void loadData();

    }


}
