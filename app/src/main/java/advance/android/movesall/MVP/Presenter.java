package advance.android.movesall.MVP;


import android.util.Log;

import javax.inject.Inject;

import advance.android.movesall.utils.BaseApplication;

public class Presenter implements Contract.Presenter {
    private Contract.View view;
    @Inject
    Model model;

    String TAG = "mtest";

    public Presenter(Contract.View view) {
        this.view = view;

        BaseApplication.getMvpComponent().inject(this);
        model.attachPresenter(this);

    }

    @Override
    public void onStartLocationServiceSelected() {
        view.startLocationService();
    }

    @Override
    public void onStopLocationServiceSelected() {
        view.stopLocationService();
    }

    @Override
    public void onStartMapActivitySelected() {
        Log.d(TAG, "onStartMapActivitySelected: in MVP presenter");

        view.startMapActivity();
    }

    @Override
    public void onDeleteTableValuesSelected() {
        model.deleteTableValues();
    }


}
