package advance.android.movesall.MVP;

import android.util.Log;

import javax.inject.Inject;

import advance.android.movesall.MVPMap.ContractMap;
import advance.android.movesall.retrofit.RetrofitInterface;
import advance.android.movesall.retrofit.SaveResponse;
import advance.android.movesall.service.LatLonModel;
import advance.android.movesall.utils.BaseApplication;
import advance.android.movesall.utils.PublicMethods;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;
import io.realm.RealmResults;

public class Model implements Contract.Model {
    private Contract.Presenter presenter;
    private ContractMap.Presenter presenterMap;
    private Realm realm = Realm.getDefaultInstance();

    @Inject
    RetrofitInterface retrofitInterface;
    private String TAG = "mtest";


    @Override
    public void attachPresenter(Contract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void deleteTableValues() {
        realm.beginTransaction();
        realm.delete(LatLonModel.class);
        realm.commitTransaction();

    }

    @Override
    public void attachPresenter(ContractMap.Presenter presenterMap) {
        this.presenterMap = presenterMap;
    }

    @Override
    public void loadData() {
        realm.beginTransaction();

        RealmResults<LatLonModel> latLonModelList = realm.where(LatLonModel.class).findAll();
        if (latLonModelList.size() != 0) {
            presenterMap.onDataLoaded(latLonModelList);
            saveMovesInfoOnServer(latLonModelList);

        }
        realm.commitTransaction();
    }

    private void saveMovesInfoOnServer(RealmResults<LatLonModel> latLonModelList) {
        BaseApplication.getRetrofitComponent().inject(this);
        if (latLonModelList.size() != 0) {

            for (LatLonModel latLonModel : latLonModelList) {

//            retrofitInterface.moveInfoSave(11.11, 22.222,"a1500", 22 , 80, "1400")


                retrofitInterface.moveInfoSave(latLonModel.getLat(), latLonModel.getLon(),
                        latLonModel.getLabel(), latLonModel.getIcon(), latLonModel.getConfidence(),
                        latLonModel.getLocation_date(), latLonModel.getSteps())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<SaveResponse>() {
                            @Override
                            public void onSubscribe(Disposable d) {
                                Log.d(TAG, "onSubscribe: d= " + d.toString());
                            }

                            @Override
                            public void onNext(SaveResponse saveResponse) {
                                Log.d(TAG, "onNext: saveResponse.getAction()=" + saveResponse.getAction());
                            }

                            @Override
                            public void onError(Throwable e) {
                                Log.d(TAG, "onError: throwable= " + e.getClass());

                            }

                            @Override
                            public void onComplete() {
                                Log.d(TAG, "onComplete: ");
                            }
                        });

            }
        }
    }

}
