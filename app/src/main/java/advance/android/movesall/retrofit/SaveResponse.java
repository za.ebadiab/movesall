package advance.android.movesall.retrofit;

import com.google.gson.annotations.SerializedName;

public class SaveResponse {

    @SerializedName("action")
    private String action;

    public void setAction(String action) {
        this.action = action;
    }

    public String getAction() {
        return action;
    }

    @Override
    public String toString() {
        return
                "SaveResponse{" +
                        "action = '" + action + '\'' +
                        "}";
    }
}