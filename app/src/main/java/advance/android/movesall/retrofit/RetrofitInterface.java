package advance.android.movesall.retrofit;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RetrofitInterface {

    @GET("MoveInfoSave.php/")
    Observable<SaveResponse> moveInfoSave(
            @Query("lat") double lat,
            @Query("lon") double lon,
//            @Query("label") String label,
            @Query(value = "label", encoded = true) String label,

            @Query("icon") int icon,
            @Query("confidence") int confidence,
//            @Query("location_date") String location_date,
            @Query(value = "location_date", encoded = true) String location_date,
            @Query("steps") long steps
    );

}
