package advance.android.movesall.dagger;

import javax.inject.Singleton;

import advance.android.movesall.retrofit.RetrofitGenerator;
import advance.android.movesall.retrofit.RetrofitInterface;
import dagger.Module;
import dagger.Provides;

@Module
public class DaggerRetrofitModule {

    @Singleton
    @Provides
    public RetrofitInterface retrofitGenerator() {
        return RetrofitGenerator.createService(RetrofitInterface.class);

    }

}
