package advance.android.movesall.dagger;

import android.app.Activity;

import javax.inject.Singleton;

import advance.android.movesall.MVP.Presenter;
import advance.android.movesall.MVPMap.PresenterMap;
import dagger.Component;

@Singleton
@Component(modules = DaggerMVPModule.class)
public interface MVPComponent {


    void inject(Presenter presenterMVP);

    void inject(PresenterMap presenterMVPMap);
}
