package advance.android.movesall.dagger;

import javax.inject.Singleton;

import advance.android.movesall.MVP.Model;
import dagger.Module;
import dagger.Provides;

@Module
public class DaggerMVPModule {


    @Singleton
    @Provides
    public Model getModel() {
        return new Model();
    }
}
