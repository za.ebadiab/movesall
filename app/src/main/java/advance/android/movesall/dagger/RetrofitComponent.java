package advance.android.movesall.dagger;


import javax.inject.Singleton;

import advance.android.movesall.MVP.Model;
import dagger.Component;

@Singleton
@Component(modules = DaggerRetrofitModule.class)
public interface RetrofitComponent {


    void inject(Model model);

}

