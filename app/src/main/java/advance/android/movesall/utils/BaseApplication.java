package advance.android.movesall.utils;

import android.app.Application;
import android.graphics.Typeface;

//import com.cedarstudios.cedarmapssdk.CedarMaps;
import com.google.android.gms.maps.GoogleMap;

import advance.android.movesall.dagger.DaggerMVPComponent;
import advance.android.movesall.dagger.DaggerMVPModule;
import advance.android.movesall.dagger.DaggerRetrofitComponent;
import advance.android.movesall.dagger.DaggerRetrofitModule;
import advance.android.movesall.dagger.MVPComponent;
import advance.android.movesall.dagger.RetrofitComponent;
import io.realm.Realm;

public class BaseApplication extends Application {

    public static BaseApplication baseApp;
    public static Typeface typeface;
    private static MVPComponent mvpComponent;
    private static RetrofitComponent retrofitComponent;

    public static MVPComponent getMvpComponent() {
        return mvpComponent;
    }

    public static RetrofitComponent getRetrofitComponent() {
        return retrofitComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        baseApp = this;
        typeface = Typeface.createFromAsset(getAssets(), Constants.appFontName);


        Realm.init(this);

        mvpComponent = DaggerMVPComponent.builder().daggerMVPModule(new DaggerMVPModule()).build();
        retrofitComponent = DaggerRetrofitComponent.builder().daggerRetrofitModule(new DaggerRetrofitModule()).build();
    }

}

