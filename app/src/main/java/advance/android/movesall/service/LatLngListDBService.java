package advance.android.movesall.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.location.DetectedActivity;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;

import advance.android.movesall.R;
import advance.android.movesall.utils.BaseApplication;
import advance.android.movesall.utils.PublicMethods;
import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;
import io.nlopez.smartlocation.activity.config.ActivityParams;
import io.nlopez.smartlocation.location.config.LocationAccuracy;
import io.nlopez.smartlocation.location.config.LocationParams;
import io.realm.Realm;

public class LatLngListDBService extends Service implements SensorEventListener {

    //     step count:
    SensorManager sManager = (SensorManager) BaseApplication.baseApp.getSystemService(Context.SENSOR_SERVICE);
    Sensor stepSensor = sManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR);
    private long steps = 0;
    LatLonModel latLonModel;

    private Realm realm = Realm.getDefaultInstance();
    String TAG = "mtest";

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

// step count:
        sManager.registerListener(this, stepSensor, SensorManager.SENSOR_DELAY_FASTEST);


        startLocationListener();

        return super.onStartCommand(intent, flags, startId);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    private void startLocationListener() {

        Log.d(TAG, "startLocationListener: before smartLocation start 1");

        LocationAccuracy trackingAccuracy = LocationAccuracy.HIGH;

        LocationParams.Builder builder = new LocationParams.Builder()
                .setAccuracy(trackingAccuracy)
                //trackingDistance:
                .setDistance(0)
                //mLocTrackingInterval :
                .setInterval(5000);
        SmartLocation.with(this)
                .location()
                .continuous()
                .config(builder.build())
                .start(new OnLocationUpdatedListener() {
                    @Override
                    public void onLocationUpdated(Location location) {
                        latLonModel = new LatLonModel();
                        latLonModel.setLat(location.getLatitude());
                        latLonModel.setLon(location.getLongitude());
                        startActivityRecognition();

                    }
                });


    }


    void saveLocationOnDB() {

        realm.beginTransaction();
        realm.copyToRealm(latLonModel);
        realm.commitTransaction();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        SmartLocation.with(this).location().stop();
        //     this.stopSelf();
        sManager.unregisterListener(this, stepSensor);


    }

    void startActivityRecognition() {

        SmartLocation.with(this).activity().config(new ActivityParams.Builder().setInterval(5000).build())
                // .start(new OnActivityUpdatedListener() {
                .start(detectedActivity -> {
                    if (detectedActivity != null) {
                        PublicMethods.Companion.toast(String.format("[From Cache] Activity %s with %d%% confidence", getNameFromType(detectedActivity), detectedActivity.getConfidence()));
                        setActivityTypeConfidence(detectedActivity);
                    } else {
                        PublicMethods.Companion.toast("Null activity");
                    }

                });

    }

    private String getNameFromType(DetectedActivity activityType) {

        switch (activityType.getType()) {
            case DetectedActivity.IN_VEHICLE:
                return "in_vehicle";
            case DetectedActivity.ON_BICYCLE:
                return "on_bicycle";
            case DetectedActivity.ON_FOOT:
                return "on_foot";
            case DetectedActivity.STILL:
                return "still";
            case DetectedActivity.TILTING:
                return "tilting";
            default:
                return "unknown";
        }
    }

    private void setActivityTypeConfidence(DetectedActivity detectedActivity) {

        switch (detectedActivity.getType()) {
            case DetectedActivity.IN_VEHICLE: {
                latLonModel.setLabel(getString(R.string.activity_in_vehicle));
                latLonModel.setIcon(R.drawable.ic_driving);
                break;
            }
            case DetectedActivity.ON_BICYCLE: {
                latLonModel.setLabel(getString(R.string.activity_on_bicycle));
                latLonModel.setIcon(R.drawable.ic_on_bicycle);
                break;
            }
            case DetectedActivity.RUNNING: {
                latLonModel.setLabel(getString(R.string.activity_running));
                latLonModel.setIcon(R.drawable.ic_running);
                break;
            }
            case DetectedActivity.STILL: {
                latLonModel.setLabel(getString(R.string.activity_still));
                latLonModel.setIcon(R.drawable.ic_still);

                break;
            }
            case DetectedActivity.TILTING: {
                latLonModel.setLabel(getString(R.string.activity_tilting));
                latLonModel.setIcon(R.drawable.ic_tilting);
                break;
            }
            case DetectedActivity.WALKING: {
                latLonModel.setLabel(getString(R.string.activity_walking));
                latLonModel.setIcon(R.drawable.ic_walking);
                break;
            }
            case DetectedActivity.UNKNOWN: {
                latLonModel.setLabel(getString(R.string.activity_unknown));
                latLonModel.setIcon(R.drawable.ic_unknown);

                break;
            }
            case DetectedActivity.ON_FOOT: {
                latLonModel.setLabel(getString(R.string.activity_on_foot));
                latLonModel.setIcon(R.drawable.ic_walking);
                break;
            }


        }

        latLonModel.setConfidence(detectedActivity.getConfidence());
        latLonModel.setLocation_date(getCurrentDate());

        saveLocationOnDB();

    }


    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {

        if (sensorEvent.sensor.getType() == Sensor.TYPE_STEP_DETECTOR) {
            //step counter
            if (sensorEvent.values.length > 0) {
                steps += sensorEvent.values[0];
                PublicMethods.Companion.toast("steps= " + steps);
                latLonModel.setSteps(steps);
                saveLocationOnDB();
            } else
                Log.d(TAG, "onSensorChanged: steps sensorEvent.values.length not> 0 ");

        }


    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
//
//
//    //function to determine the distance run in kilometers using average step length for men and number of steps
//    public float getDistanceRun(long steps){
//        float distance = (float)(steps*78)/(float)100000;
//        return distance;
//    }


    String getCurrentDate() {


        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        try {
            return URLEncoder.encode(sdf.format(new Date()), "UTF-8");
        } catch (Exception e) {
            return "";
        }


    }


}
