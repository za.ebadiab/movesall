package advance.android.movesall.MVPMap;


import java.util.List;


import javax.inject.Inject;

import advance.android.movesall.MVP.Model;
import advance.android.movesall.service.LatLonModel;
import advance.android.movesall.utils.BaseApplication;

public class PresenterMap implements ContractMap.Presenter {
    private ContractMap.View viewMap;
    @Inject
    Model model;

    public PresenterMap(ContractMap.View viewMap) {
        this.viewMap = viewMap;

        BaseApplication.getMvpComponent().inject(this);
        model.attachPresenter(this);
    }

    @Override
    public void loadData() {
        model.loadData();
    }

    @Override
    public void onDataLoaded(List<LatLonModel> latLonModel) {
        viewMap.onDataLoaded(latLonModel);
    }
}