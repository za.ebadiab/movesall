package advance.android.movesall.MVPMap;

import java.util.List;

import advance.android.movesall.service.LatLonModel;

public interface ContractMap {

    interface View {
        void onDataLoaded(List<LatLonModel> latLonModel);

    }

    interface Presenter {

        void loadData();

        void onDataLoaded(List<LatLonModel> latLonModel);

    }

}
