package advance.android.movesall;

import android.graphics.Color;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.List;

import advance.android.movesall.MVPMap.ContractMap;
import advance.android.movesall.MVPMap.PresenterMap;
import advance.android.movesall.service.LatLonModel;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, ContractMap.View {
    PresenterMap presenterMap = new PresenterMap(this);
    private GoogleMap mMap;
    String TAG = "mtest";
    TextView footCount;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        bind();
    }

    void bind(){
        footCount = findViewById(R.id.footCount);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;


        mMap.getUiSettings().setZoomControlsEnabled(true);

        presenterMap.loadData();

    }

    @Override
    public void onDataLoaded(List<LatLonModel> latLonModelList) {

      //  samplePoints();


        // show on Map
        if (latLonModelList.size() != 0) {
            addLines(latLonModelList);
        }

    }


    public void addLines(List<LatLonModel> latLonModelList) {

        ArrayList<LatLng> latLngArrayList = changeModelListToLatLonList(latLonModelList);
        if (latLngArrayList.size() != 0) {

//            for (int i=0 ; i<latLonModelList.size()-1 && i<316 ; i++) {
            for (int i=0 ; i<latLonModelList.size()-1 ; i++) {
                Log.d(TAG, "addPolyline: latLonModelList.size()= "+latLonModelList.size()+" i= "+i);

                mMap.addPolyline((new PolylineOptions())
                        .add(latLngArrayList.get(i))
                        .add(latLngArrayList.get(i + 1))
                        .width(10)
                        .color(getColorByActivityLabel(latLonModelList.get(i+1)))
                        .geodesic(true));
//            }
//
//            for (int i=0 ; i<latLonModelList.size()-1 ; i++) {
//
//                start = i;
//                for (int j=i ; j<latLonModelList.size()-1 ; j++) {
//
//                if (latLonModelList.get(j).getLabel().equals(latLonModelList.get(j + 1).getLabel())) {
//
//                    end = j + 1;
//
//
//                } else {
//                    end= j;
//
//                    break;
//                }
//
//                    i=end+1;
//                }
//
//                position = start+ Math.round(end - start)/2;

//                BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(latLonModelList.get(end).getIcon());
//                Log.d(TAG, "addLines: marker lebal= " + latLonModelList.get(end).getLabel() + " end= " + end+ " position= "+position+"  start= "+start+" i= "+i);
//
//                mMap.addMarker(new MarkerOptions().icon(icon).position(latLngArrayList.get(position)).title(latLonModelList.get(end).getLabel()));

                BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(latLonModelList.get(i).getIcon());
                mMap.addMarker(new MarkerOptions().icon(icon).position(latLngArrayList.get(i)).title(latLonModelList.get(i).getLabel()));

            }
            BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(latLonModelList.get(latLonModelList.size()-1).getIcon());
            mMap.addMarker(new MarkerOptions().icon(icon).position(latLngArrayList.get(latLonModelList.size()-1)).title(latLonModelList.get(latLonModelList.size()-1).getLabel()));


            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLngArrayList.get(0)));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(16));

            mMap.getUiSettings().setZoomControlsEnabled(true);


      //      footCount.setText("تعداد کل قدم ها: "+latLonModelList.get(latLonModelList.size()-1).getSteps());
            footCount.setText(getString(R.string.activity_stepCounts)+latLonModelList.get(latLonModelList.size()-1).getSteps());
        }




    }


    ArrayList<LatLng> changeModelListToLatLonList(List<LatLonModel> latLongModelList) {
        ArrayList<LatLng> latLngList = new ArrayList<>();


        for (LatLonModel model : latLongModelList) {
            latLngList.add(new LatLng(model.getLat(), model.getLon()));
        }

        return latLngList;
    }

//    void samplePoints(){
//
//        LatLonModel latLonModel = new LatLonModel();
//        latLonModel.setLat(35.72483);
//        latLonModel.setLon(51.42117);
//        latLonModel.setConfidence(70);
//        latLonModel.setLabel(getString(R.string.activity_still));
//        latLonModel.setIcon(R.drawable.ic_still);
//        latLonModel.setLocation_date("1400");
//
//
//
//
//        LatLonModel latLonModel2 = new LatLonModel();
//        latLonModel2.setLat(35.72397);
//        latLonModel2.setLon(51.42119);
//        latLonModel2.setConfidence(80);
//        latLonModel2.setLabel(getString(R.string.activity_walking));
//        latLonModel2.setIcon(R.drawable.ic_walking);
//        latLonModel2.setLocation_date("1401");
//
//
//        LatLonModel latLonModel3 = new LatLonModel();
//        latLonModel3.setLat(35.72411);
//        latLonModel3.setLon(51.42219);
//        latLonModel3.setConfidence(70);
//        latLonModel3.setLabel(getString(R.string.activity_tilting));
//        latLonModel3.setIcon(R.drawable.ic_tilting);
//        latLonModel3.setLocation_date("1403");
//
//
//        LatLonModel latLonModel4 = new LatLonModel();
//        latLonModel4.setLat(35.72484);
//        latLonModel4.setLon(51.42177);
//        latLonModel4.setConfidence(70);
//        latLonModel4.setLabel(getString(R.string.activity_running));
//        latLonModel4.setIcon(R.drawable.ic_running);
//        latLonModel4.setLocation_date("1400");
//
//        LatLonModel latLonModel5 = new LatLonModel();
//        latLonModel5.setLat(35.72384);
//        latLonModel5.setLon(51.42277);
//        latLonModel5.setConfidence(70);
//        latLonModel5.setLabel(getString(R.string.activity_running));
//        latLonModel5.setIcon(R.drawable.ic_running);
//        latLonModel5.setLocation_date("1400");
//
//
//
//
//        latLonModelList.add(0 , latLonModel);
//        latLonModelList.add(1, latLonModel2);
//        latLonModelList.add(2, latLonModel3);
//        latLonModelList.add(3, latLonModel4);
//        latLonModelList.add(4, latLonModel5);
//    }

    private int getColorByActivityLabel(LatLonModel latLonModel) {
        if (latLonModel.getLabel() != null) {


            if (latLonModel.getLabel().equals(getString(R.string.activity_in_vehicle))) {
                return Color.LTGRAY;
            } else if (latLonModel.getLabel().equals(getString(R.string.activity_on_bicycle))) {
                return Color.DKGRAY;
            }
        else if (latLonModel.getLabel().equals(getString(R.string.activity_on_foot))) {
                return Color.BLUE ;
        }
            else if (latLonModel.getLabel().equals(getString(R.string.activity_running))) {
                return Color.RED;
            } else if (latLonModel.getLabel().equals(getString(R.string.activity_still))) {
                return Color.GRAY;
            } else if (latLonModel.getLabel().equals(getString(R.string.activity_tilting))) {
                return Color.YELLOW;
            } else if (latLonModel.getLabel().equals(getString(R.string.activity_walking))) {
                return Color.GREEN;
            } else if (latLonModel.getLabel().equals(getString(R.string.activity_unknown))) {
                return Color.GREEN;
            }
        }
        return Color.RED;
    }



}



